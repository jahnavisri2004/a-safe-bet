import sys

def cell(row, col, fs, bs):
    box = [[0] * col for i in range(row)]
    for i, j, _ in fs:
        if i <= row and j <= col :      #if u give 5 rows and slash is palced at 6th row mean then that is not possible.
            box[i-1][j-1] = '/'
    for i, j, _ in bs:
        if i <= row and j <= col:
            box[i-1][j-1] = '\\'
    return box

def empt_cells(row, col, box) :
    count = 0
    for i in range(row) :
        for j in range(col) :
            if box[i][j] == 0 :    #if cell value is 0 then count is incremented.
                count += 1
     return count == row*col :

def missing_mir_count(row,col,box) :
    count = 0
    pos = ()
    for i in range(row - 1) :
        for j in range(col - 1) :
            if box[i][j] != 0 and (box[i + 1][j + 1] != 0 or box[i + 1][j] != 0 or box[i][j + 1] != 0) :
                count += 1
                pos = (i + 1, j + 1)
    return count, pos

        #taking inputs using command line arguements
def solve(r, c, m, n, slash, backslash):
state = cell(r, c, slash, backslash)

    if empt_cells(r, c, state):
        return "impossible"

    count, pos = missing_mir_count(r, c, state)
    if count == 0:
        return "0"
    elif count == 1:
        return f"1 {pos[0]} {pos[1]}"
    else:
        return f"{count} {pos[0]} {pos[1]}"
                print(i,end = ' ')
        else:
            print(return_posn(row,col,safe))
inp = sys.stdin.readlines()
r,c,m,n = map(int,inp[0].split())
slash = [tuple(map(int, inp[i].split())) + (0,) for i in range(1,m + 1)]
backslash = [tuple(map(int,inp[i].split())) + (0,) for i in range(1,n + 1)]

print(solve(r, c, m, n, slash, backslash)

